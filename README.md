
# Visualizing Special Relativity

This is a Processing.py program that visualizes the effects of special relativity. It shows the way that spacetime transforms when the observer nears the speed of light. Controls are simple: you click to place and remove events, and use the arrow keys to change your frame of reference.

![](Media/lots-of-particles.gif)

In this image, the vertical direction is time (up is the future, down is the past), and the horizontal direction is space (which is 1D for simplicity). What we're looking at is the universe as described by an observer at a particular point in space and time. The point that this observer is at is right smack-dab in the center of the diagram, where the two black diagonal lines meet. These lines represent the observer's light cone: the paths through spacetime that would be taken by beams of light emitted in either direction. And finally, the multicolored dots scattered in the upper quadrant represent other spacetime events in the observer’s future. This visualization is based on Minkowsky diagrams, described in more detail [here](https://en.wikipedia.org/wiki/Spacetime_diagram#Minkowski_diagrams).

What's being varied is the velocity of the observer. The observer is not actually moving through time in this visualization. What is being shown is the way that other events would be arranged spatially and temporally if the observer had different velocities.

## Temporal order

![](Media/RGB-Transform.gif)

You can see many effects in this short gif. 

1. The light cone's slope doesn't change. This represents that the speed of light is constant.
2. The temporal positions of objects depend on your velocity. Objects to your future right don't just get further away spatially when you move away from them, they also get further away temporally!
3. Simultaneity is relative. When the velocity is zero, Red and Blue are at the same moment of time. But when the velocity is greater than zero, Red falls behind Blue in temporal order. In fact, you can find a velocity that makes any two of these three points simultaneous!
4. The temporal order of events is relative. The possible orderings of events that you can observe include RGB, GRB, GBR, and BGR. See if you can spot them all!

Notice that we didn't get all possible temporal ordering. In particular we didn't have RBG or BRG. It turns out that in general, there are some constraints we can place on temporal orderings, related to the light cones of the events in question. You can see those visualized here:

![](Media/RGB-with-Light-Cones.gif)

Two things to notice: First, all three events are outside each others’ light cones. And second, no event ever crosses over into another event’s light cone. This makes some intuitive sense, and gives us a constraint that will hold true in all reference frames: Events that are outside each others’ light cones from one perspective, are outside each others’ light cones from all perspectives. Same thing for events that are inside each others’ light cones.

Conceptually, events being inside each others’ light cones corresponds to them being in causal contact. So another way we can say this is that all observers will agree on what the possible causal relationships in the universe are.

The following visualization demonstrates that events in causal contact cannot switch temporal order:

![](Media/RGB-Causal.gif)

Blue is fully contained inside the future light cone of Red, and no matter what frame of reference we choose, it cannot escape this. 

The ambiguity of temporal order is even more starkly demonstrated when you have more events:

![](Media/Order-of-Many-Events.gif)

You can also see the effect on the temporal order with events that are outside your future light cone:

![](Media/Outside-the-Light-Cone.gif)

Here we see an even more strange effect: When considering the temporal position of events outside your own light cone, there is ambiguity about whether these events are in your future or past! This can be seen very clearly with just two events:

![](Media/Future-Past-Swap.gif)

In the v = 0 frame of reference, Red and Green are simultaneous with you. But for v > 0, both are in your past, and for v < 0, both are in your future.

The rule mentioned earlier about events in causal contact having a consistent temporal ordering still holds true when considering events outside your event horizon, as can be seen in the following:

![](Media/Straddling-No-Cause.gif)

![](Media/Straddling-With-Cause.gif)

## Spatial Distortions

We can also use the program to set up an object sitting at rest some distance away from you.

![](Media/object-at-rest.png)

What does this look like if we are moving towards the object? Here's what we see at 80% of the speed of light:

![](Media/object-at-rest-moving.png)

The object now rushes towards us from our frame of reference, and quickly passes us by and moves off to the left. But notice the spatial distortion in the image! At the present moment (t = 0), the object looks significantly closer than it was previously. 

This shows that whether I am moving towards an object or standing still appears to change how far away the object presently is. This is the famous phenomenon of <strong>length contraction</strong>. If we imagine placing two objects at different distances from us, each at rest with respect to the v = 0 frame, then moving towards them would result in both of them getting closer to us as well as each other, and thus shrinking! 

![](Media/Contraction.gif)

Evidently, when we move the universe shrinks!

One final effect we can notice is that while length contraction results in the object getting closer to you, it also results in the observed distance between yourself and the object increasing! Why? Well, what you observe is dictated by the beams of light that make it to your eye. So at the moment t = 0, what you are observing is everything along the two diagonals in the bottom half of the images. And in the second image, where you are moving towards the object, the place where the object and diagonal intersect is much further away than it is in the first image! Evidently, moving towards an object makes it appear further away, even though in reality it is getting closer to you! You can see this effect visualized in three dimensions in MIT Gamelab's [Slower Speed of Light project](http://gamelab.mit.edu/games/a-slower-speed-of-light/).

This holds as a general principle. The reason? When you observe an object, you are really observing it as it was some time in the past (however much time it took for light to reach your eye). And when you move towards an object, that past moment you are observing falls further into the past. (This is sort of the flip-side of time dilation.) Since you are moving towards the object, looking further into the past means looking at the object when it was further away from you. And so therefore the object ends up appearing more distant from you than before!


