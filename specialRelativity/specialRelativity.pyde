
def initiateThree():
    X, T = [-50, 0, 50], [100, 90, 110]
    colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
    return T, X, colors

def initiateObjectAtRest(N, distance):
    X = [distance]*N
    T = [-250 + i*500/N for i in range(N)]
    colors = [(200, 100, 100) for _ in range(N)]
    return T, X, colors

w, h = 900, 600

v = 0
T, X, colors = initiateThree()

showLightCones, showTrace, showTimeOrder, showInfoBox = False, False, True, True
overButton1, overButton2, overButton3, overButton4 = False, False, False, False
overInfoBox = False

trace = PGraphics()

def setup():
    global trace
    size(w, h)
    stroke(0)
    trace = createGraphics(width, height)
    
def draw():
    global v, trace
    global overButton1, overButton2, overButton3, overButton4, overInfoBox
    
    overButton1 = w - 115 < mouseX < w - 7 and 9 < mouseY < 24
    overButton2 = w - 85 < mouseX < w - 7 and 25 < mouseY < 38
    overButton3 = w - 105 < mouseX < w - 7 and 39 < mouseY < 53
    overButton4 = w - 105 < mouseX < w - 7 and 54 < mouseY < 68
    overInfoBox = w/2 - 160 < mouseX < w/2 + 135 and 5 < mouseY < 70
    rect(-160, 70 - h/2, 295, -65)
    
    newX = [boost(X[i], T[i], v)[0] for i in range(len(X))]
    newT = [boost(X[i], T[i], v)[1] for i in range(len(X))]
    
    if keyPressed:
        if keyCode == RIGHT or key == "d":
            v += (1 - v)/100.
        if keyCode == LEFT or key == "a":
            v -= (1 + v)/100.
        if keyCode == SHIFT:
            if mousePressed:
                x, t = boost(mouseX - w/2, h/2 - mouseY, -v)
                X.append(x)
                T.append(t)
                colors.append((random(0, 255), random(0, 255), random(0, 255)))
    
    background(255)
    if showTrace:
        image(trace, 0, 0)
    translate(w/2, h/2)
  
    drawAxes()
    if showLightCones:
        drawLightCones(newX, newT)
    drawTrace(newX, newT)
    drawEvents(newX, newT)
    if showTimeOrder:
        showTemporalOrder(newT, colors)
    drawButtons()
    
    fill(0)
    text("Velocity as a percentage of the speed of light = " + 
         str(int(v*100)) + "%", -155, .4*h)

def mouseReleased():
    global v, X, T, colors, trace, showLightCones, showTrace, showTimeOrder, showInfoBox
    overEvent = False
    overWhichEvent = 0
    newX = [boost(X[i], T[i], v)[0] for i in range(len(X))]
    newT = [boost(X[i], T[i], v)[1] for i in range(len(X))]
    for i in range(len(X)):
        if newX[i] - 6 < mouseX - w/2 < newX[i] + 6:
           if newT[i] - 6 < h/2 - mouseY < newT[i] + 6:
               overEvent = True
               overWhichEvent = i
    
    if overButton1:
        showLightCones = False if showLightCones else True
    elif overButton2:
        showTrace = False if showTrace else True
        trace.beginDraw()
        trace.background(255)
        trace.endDraw()
    elif overButton3:
        X, T, colors = [], [], []
    elif overButton4:
        showTimeOrder = False if showTimeOrder else True
    elif overInfoBox and showInfoBox:
        showInfoBox = False
    elif overEvent:
        del X[overWhichEvent], T[overWhichEvent], colors[overWhichEvent]
    else:
        x, t = boost(mouseX - w/2, h/2 - mouseY, -v)
        X.append(x)
        T.append(t)
        colors.append((random(0, 255), random(0, 255), random(0, 255)))

def boost(x, t, v):
    gamma = 1/sqrt(1 - sq(v))
    xNew = gamma * (x - v*t)
    tNew = gamma * (t - v*x)
    return xNew, tNew
    
def drawButtons():
    stroke(255)

    fill(150 if overButton1 else 100)
    rect(w/2 - 115, 24 - h/2, 108, -15)
    fill(150 if overButton2 else 100)
    rect(w/2 - 85, 38 - h/2, 78, -13)
    fill(150 if overButton3 else 100)
    rect(w/2 - 105, 53 - h/2, 98, -14)
    fill(150 if overButton4 else 100)
    rect(w/2 - 110, 68 - h/2, 103, -14)
    
    if showInfoBox:
        stroke(0)
        fill(220 if overInfoBox else 200)
        rect(-160, 70 - h/2, 295, -65)
        fill(0)
        text("Click to place and remove events!", -110, 20 - h/2)
        text("Shift click to place multiple events", -110, 35 - h/2)
        text("Use the left and right arrows to change velocity", -152, 50 - h/2)
    
    fill(255)
    text("Event light cones", w/2 - 110, 20 - h/2)
    text("Show traces", w/2 - 80, 35 - h/2)
    text("Clear all events", w/2 - 100, 50 - h/2)
    text("Show time order", w/2 - 106, 65 - h/2)

def drawAxes():
    strokeWeight(2)
    stroke(0)
    line(-w/2, w/2, w/2, -w/2)
    line(w/2, w/2, -w/2, -w/2)
    strokeWeight(.5)
    line(0, 0, -v*w/2, -w/2)

def drawLightCones(x, t):
    for i in range(len(x)):
        strokeWeight(.3)
        stroke(*colors[i])
        line(x[i], -t[i], x[i] - w, -t[i] - w)
        line(x[i], -t[i], x[i] + w, -t[i] - w)
        
def drawTrace(x, t):
    for i in range(len(x)):
        trace.beginDraw()
        trace.translate(w/2, h/2)
        trace.strokeWeight(1)
        trace.stroke(*colors[i])
        trace.point(x[i], -t[i])
        trace.endDraw()
        
def drawEvents(x, t):
    strokeWeight(1)
    stroke(0)
    for i in range(len(x)):
        fill(*colors[i])
        ellipse(x[i], -t[i], 10, 10)
            
def showTemporalOrder(tNew, colors):
    fill(0)
    strokeWeight(1)
    text("Temporal ordering", -57, h/2 - 38)
    order = eventOrder(tNew)
    stroke(0)
    for i in range(len(order)):
        fill(*colors[order[i]])
        rect(10*(i - len(order)//2), h/2 - 30, 8, 8)
        
def eventOrder(alist):
    copied = [element for element in alist]
    copied.sort()
    return [alist.index(element) for element in copied]
